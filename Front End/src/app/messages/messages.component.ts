// Npm dependencies
import { Component, ViewChild }        from '@angular/core';
import { NgForm }                      from '@angular/forms';
import { Router }                      from '@angular/router';

// Service imports
import { ServiceService }              from '../service.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})

/**
 * This component holds the message part.
 */
export class MessagesComponent {
  
  @ViewChild("form")  messageFrom : NgForm;
  constructor( private service:ServiceService, private router : Router) { }

  sendMessage(){
    //Sending message to service method.
    this.service.sendMessage(this.messageFrom.value);
    //Savigating to base page
    this.router.navigate(["/"])
  }
}
