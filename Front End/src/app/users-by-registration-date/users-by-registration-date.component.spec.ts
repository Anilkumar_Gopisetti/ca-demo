import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersByRegistrationDateComponent } from './users-by-registration-date.component';

describe('UsersByRegistrationDateComponent', () => {
  let component: UsersByRegistrationDateComponent;
  let fixture: ComponentFixture<UsersByRegistrationDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersByRegistrationDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersByRegistrationDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
