import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-users-by-registration-date',
  templateUrl: './users-by-registration-date.component.html',
  styleUrls: ['./users-by-registration-date.component.css']
})
export class UsersByRegistrationDateComponent implements OnInit {
  userData : any;
  constructor(private service : ServiceService) { }

  ngOnInit() {

    if(!localStorage.getItem('token')){
      alert("please login..!");
      window.location.href = "/login";
    }

    this.service.getUsersByRegisterDate().subscribe((data) => {
      this.userData = data.response; 
      console.log(this.userData)
    })
  }

}
