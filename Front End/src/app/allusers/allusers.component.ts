//Npm dependencies
import { Component, OnInit } from '@angular/core';

//service imports
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-allusers',
  templateUrl: './allusers.component.html',
  styleUrls: ['./allusers.component.css']
})

/**
 * This component holds the users list and sorted users list.
 */
export class AllusersComponent implements OnInit {
  public userData: Array<any>;
  sortedData = new Array();
  private page: number = 0;
  private pages: Array<number>
  constructor(private service: ServiceService) { }

  setPage(i) {
    this.page = i;
    this.getUsers();
  }

  ngOnInit() {
    if(!localStorage.getItem('token')){
      alert("please login..!");
      window.location.href = "/login";
    }
    
    this.getUsers();
    /**
     * Calling service method for getting page number count .
     */
    this.service.getPageCount().subscribe((data) => {
      this.pages = new Array(data['response']);
    })
  }
  /**
   * This method gets all the users.
   */
  getUsers() {
    /**
     * Calling service method for getting users .
     */
    this.service.getAllUsers(this.page).subscribe((data) => {
      this.userData = data['response'];
      this.sortedData.length = 0;
      for (let i = 0; i < data.response.length; i++) {
        this.sortedData.push(data.response[i].firstName);
      }
      this.sortedData.sort();

    /**
     * Calling service method for getting sorted users list .
     */
      this.service.getName(this.sortedData);
    })
  }
}
