//Npm dependencies
import { Component, OnInit } from '@angular/core';
//service imports
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
/**
 * This component displays the user profile details.
 */
export class UserDetailsComponent implements OnInit {
  userData:any;
  constructor(private service:  ServiceService) { }

  ngOnInit() {
    if(!localStorage.getItem('token')){
      alert("please login..!");
      window.location.href = "/login";
    }
    
    this.service.getUserProfile().subscribe((data)=> {
      console.log(data.response);
      this.userData = data.response;
    })
  }
}
