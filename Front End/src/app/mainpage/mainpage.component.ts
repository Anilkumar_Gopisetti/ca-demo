// Npm dependencies
import { Component, OnInit }          from '@angular/core';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})

/**
 * This component holds the landing page of application.
 */
export class MainpageComponent implements OnInit {
  date: any;
  constructor() { }

  ngOnInit() {
    //Getting present date.
    this.date = new Date().toLocaleString();
  }

}
