// Npm dependencies
import { Component, OnInit } from '@angular/core';

// Service imports
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

/**
 * This component is for searching the user.
 */
export class SearchComponent implements OnInit {
  username : any;
  userDetails : any;
  noUserFound = false;
  constructor( private service : ServiceService ) { }

  ngOnInit(){
    if(!localStorage.getItem('token')){
      alert("please login..!");
      window.location.href = "/login";
    }
  }
  search(){
    /**
     * Calling service method for getting the details based on username  .
     */
    console.log(this.username)
    this.service.search(this.username).subscribe((data) => {
      if(data.statusCode == 1){
      this.userDetails = data.response;
      this.noUserFound = false;
      }
      else{
        // alert("no user found with this username")
        this.noUserFound = true;
      }
    })
  }
}
