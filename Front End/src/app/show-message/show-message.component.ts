//Npm dependencies
import { Component, OnInit }      from '@angular/core';

//service imports
import { ServiceService }         from '../service.service';

@Component({
  selector: 'app-show-message',
  templateUrl: './show-message.component.html',
  styleUrls: ['./show-message.component.css']
})

/**
 * This component holds the message to be displayed in main page
 */
export class ShowMessageComponent implements OnInit {
  msgData: any;
  constructor(private service: ServiceService) { }

  ngOnInit() {
    //Calling service method to get the message details.
    this.msgData = this.service.getMessage();
  }

}
