//Npm dependencies
import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormBuilder }     from '@angular/forms';
import { Router }                     from '@angular/router';

//Service imports
import { ServiceService }             from '../service.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})

/**
 * This component holds deleting user operation.
 */
export class DeleteUserComponent implements OnInit {

  deleteForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private service: ServiceService, private routes: Router) {
    this.deleteForm = this.formBuilder.group({
      username: [''],
    })
  }

  ngOnInit() {
    if(!localStorage.getItem('token')){
      alert("please login..!");
      window.location.href = "/login";
    }
  }
  /**
    * This method is to delete the user based on username
    * @param data 
    */
  deleteUser() {
    /**
    * Calling service method for deleting the user based on username.
    */
    this.service.deleteUser(this.deleteForm.value).subscribe((data: any) => {
      if (data.statusCode == 1) {
        alert("User Deleted Successfully");
        window.location.reload();
      }
      else {
        alert("No user found with this username:  " + this.deleteForm.value.username)
        window.location.reload();
      }
    })
  }

}
