//Npm dependencies
import { Component, OnInit }        from '@angular/core';
import { Router }                   from '@angular/router';
import { FormGroup }                from '@angular/forms';

//Service imports
import { ServiceService }           from '../service.service';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})

/**
 * This component is used to edit the details of users
 */
export class EditUserComponent implements OnInit {

  userData: any;
  phone: string;
  firstName: string;
  lastName: string;
  username:string;
  editForm: FormGroup;
 

  constructor(private service: ServiceService, private routes: Router) { }


  ngOnInit() {

    if(!localStorage.getItem('token')){
      alert("please login..!");
      window.location.href = "/login";
    }
    
    /**
     * Calling service method for getting the user details by id.
     */
    this.service.getUserProfile().subscribe((data) => {
      this.userData = data.response;
    })
 
  }

  update() {
    /**
     * Calling service method for updating the user details by id.
     */
    this.service.editUser(this.userData).subscribe((data: any) => {
      console.log(this.userData)
      console.log(data)
      if (data.statusCode == 1) {
        alert("user details updated successfully");
        //Navigating to userdetails page.
        window.location.href = "/userDetails";
      }
      else {
        alert("user Details not updated");
      }
    })
  }
}
