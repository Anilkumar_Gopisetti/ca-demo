// Npm dependencies
import { Component, OnInit }             from '@angular/core';
import { FormGroup, FormBuilder }        from '@angular/forms';
import { Router }                        from '@angular/router';

// Service imports
import { ServiceService }                from '../service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

/**
 * This component is for user registration.
 */
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private service: ServiceService, private routes: Router) {
    this.registerForm = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      username: [''],
      password: [''],
      email: [''],
      phone: [''],
    })
  }

  ngOnInit() {
  }

  /**
   * Registration form submission.
   */
  doRegister() {
    //Calling service method by sending the username.
    this.service.sendUserName(this.registerForm.value.username);
    //Calling service method by sending the form details.
    this.service.doregistration(this.registerForm.value).subscribe((data) => {
      //checking the status code
      if (data.success == true) {
        //navigating to success page
        this.routes.navigate(["/success"]);
      }
      else {
        alert("registration failed")
      }
    })
  }
}
