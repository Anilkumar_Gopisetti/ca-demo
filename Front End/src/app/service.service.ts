//Npm dependencies
import { Injectable }                    from '@angular/core';
import { HttpClient, HttpHeaders }       from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

/**
 * This is service component and api calls takes place here.
 */
export class ServiceService {

  //assigning the url path to a variable
  url: string = "http://192.168.150.51:8080/api/";

  msgData = [];
  sortedNames = [];
  userName: string = "";
  token = localStorage.getItem("token");
  httpoptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': "Bearer " + this.token
    })
  }
  constructor(private http: HttpClient) { }

  /**
   * This method is for login.
   * @param loginData 
   */
  doLogin(data) {
    let body = {
      "usernameOrEmail": data.username,
      "password": data.password,
    }
    return this.http.post<any>(this.url + "auth/signin", body)
  }

  /**
   * This method is for registration.
   * @param registerData 
   */
  doregistration(registerData) {
    let body = {
      "firstName": registerData.firstName,
      "lastName": registerData.lastName,
      "username": registerData.username,
      "password": registerData.password,
      "email": registerData.email,
      "phone": registerData.phone,
    }
    return this.http.post<any>("http://192.168.150.51:8080/api/auth/signup", body)
  }

  /**
    * This method is used to search the user by username.  
    */
  search(data) {
    console.log(data);
    return this.http.get<any>(this.url + "admin/getUserByUsername/" + data,this.httpoptions)
  }

  /**
   * This method is used to get the users.  
   */
  getAllUsers(page: number) {
    return this.http.get<any>(this.url + "admin/getAllUsers/" + page,this.httpoptions);
  }
  /**
   * This method is used to get the user list by registration date .  
   */
  getUsersByRegisterDate() {
    
    return this.http.get<any>(this.url + "admin/getAllUsers/registrationDate", this.httpoptions);
  }
  // /**
  //  * This method is used to get the user profile.  
  //  */
  getUserProfile() {
    return this.http.get<any>(this.url + "user/getUserByUsername/" + localStorage.getItem("name"),this.httpoptions);
  }

  /**
    * This method is used to delete the user.  
    */
  deleteUser(data) {
    return this.http.delete(this.url + "admin/deleteUser/" + data.username);
  }
  /**
     * This method is used to get the count of page numbers for pagination.  
     */
  getPageCount() {
    return this.http.get(this.url + 'admin/getCountPerPage',this.httpoptions);
  }

  /**
   * This method is used to edit the users.  
   */
  editUser(data) {
    let body = {
      "firstName": data.firstName,
      "lastName": data.lastName,
      "username": data.username,
      "phone": data.phone,
    }
    console.log(body)
    return this.http.put<any>(this.url + 'user/updateUserDetails', body,this.httpoptions)
  }

  /**
   * This method is used to edit the depaartment of user.  
   */
  updateDepartment(data) {
    return this.http.put<any>(this.url + "admin/updateDepartment/" + data.id + "/" + data.department, null);
  }
  /**
   * This method is used to store the message..  
   */
  sendMessage(data: any) {
    this.msgData.push(data);
    console.log(this.msgData)

  }
  /**
   * This method is used to get the message details.  
   */
  getMessage() {
    return this.msgData;
  }
  /**
   * This method is used to store the names.  
   */
  getName(data) {
    this.sortedNames.push(data);
  }
  /**
    * This method is used to get the names.  
    */
  sendNames() {
    return this.sortedNames;
  }
  /**
   * This method is used to store the username.  
   */
  sendUserName(name) {
    this.userName = name;
  }
  /**
    * This method is used to get the username.  
    */
  getUserName() {
    return this.userName;
  }

}
