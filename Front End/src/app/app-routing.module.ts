//Npm dependencies
import { NgModule }                           from '@angular/core';
import { Routes, RouterModule }               from '@angular/router';

//Component dependencies
import { MainpageComponent }                  from './mainpage/mainpage.component';
import { AboutComponent }                     from './about/about.component';
import { RegisterComponent }                  from './register/register.component';
import { AllusersComponent }                  from './allusers/allusers.component';
import { SearchComponent }                    from './search/search.component';
import { SuccessComponent }                   from './success/success.component';
import { MessagesComponent }                  from './messages/messages.component';
import { LoginComponent }                     from './login/login.component';
import { UserDetailsComponent }               from './user-details/user-details.component';
import { PoliciesComponent }                  from './policies/policies.component';
import { SetComponent }                       from './set/set.component';
import { DeleteUserComponent }                from './delete-user/delete-user.component';
import { EditUserComponent }                  from './edit-user/edit-user.component';
import { UsersByRegistrationDateComponent }   from './users-by-registration-date/users-by-registration-date.component';

//Route paths
const routes: Routes = [
  { path: "", component: MainpageComponent },
  { path: "login", component: LoginComponent },
  { path: "about", component: AboutComponent },
  { path: "register", component: RegisterComponent },
  { path: "users", component: AllusersComponent },
  { path: "search", component: SearchComponent },
  { path: "success", component: SuccessComponent },
  { path: "message", component: MessagesComponent },
  { path: 'userDetails', component: UserDetailsComponent },
  { path: 'policies', component: PoliciesComponent },
  { path: 'allocation', component: SetComponent },
  { path: 'delete', component: DeleteUserComponent },
  { path: 'edit', component: EditUserComponent },
  { path: 'usersByregistration', component: UsersByRegistrationDateComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
