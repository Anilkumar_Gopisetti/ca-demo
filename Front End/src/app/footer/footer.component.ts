//Npm dependencies
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})

/**
 * This component holds the data to be displayed in footer.
 */
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
