//Npm dependencies
import { BrowserModule }                         from '@angular/platform-browser';
import { NgModule }                              from '@angular/core';
import { AppRoutingModule }                      from './app-routing.module';
import { FormsModule, ReactiveFormsModule }      from '@angular/forms'
import { HttpClientModule }                      from '@angular/common/http';
import { SortPipe }                              from './sort.pipe';
import { NgxPaginationModule }                   from 'ngx-pagination';

//Component dependencies
import { AppComponent }                          from './app.component';
import { MainpageComponent }                     from './mainpage/mainpage.component';
import { HeaderComponent }                       from './header/header.component';
import { FooterComponent }                       from './footer/footer.component';
import { MenuComponent }                         from './menu/menu.component';
import { RegisterComponent }                     from './register/register.component';
import { AboutComponent }                        from './about/about.component';
import { AllusersComponent }                     from './allusers/allusers.component';
import { SearchComponent }                       from './search/search.component';
import { SortingComponent }                      from './sorting/sorting.component';
import { SuccessComponent }                      from './success/success.component';
import { MessagesComponent }                     from './messages/messages.component';
import { ShowMessageComponent }                  from './show-message/show-message.component';
import { LoginComponent }                        from './login/login.component';
import { UserDetailsComponent }                  from './user-details/user-details.component';
import { SetComponent }                          from './set/set.component';
import { DeleteUserComponent }                   from './delete-user/delete-user.component';
import { EditUserComponent }                     from './edit-user/edit-user.component'
import { UsersByRegistrationDateComponent }      from './users-by-registration-date/users-by-registration-date.component';
import { PoliciesComponent }                     from './policies/policies.component';



@NgModule({
  declarations: [
    AppComponent,
    MainpageComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    RegisterComponent,
    AboutComponent,
    AllusersComponent,
    SearchComponent,
    SortingComponent,
    SortPipe,
    SuccessComponent,
    MessagesComponent,
    ShowMessageComponent,
    LoginComponent,
    UserDetailsComponent,
    SetComponent,
    DeleteUserComponent,
    UsersByRegistrationDateComponent,
    EditUserComponent,
    PoliciesComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
