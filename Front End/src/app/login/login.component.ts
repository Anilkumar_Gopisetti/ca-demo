// Npm dependencies
import { Component, OnInit }             from '@angular/core';
import { FormBuilder, FormGroup }        from '@angular/forms';
import { Router }                        from '@angular/router';

// Service imports
import { ServiceService }                from '../service.service';
import { HttpClient }                    from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

/**
 * This component is for user Login.
 */
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  userData:any;
  constructor(private formBuilder: FormBuilder, private service: ServiceService, private routes: Router,private http: HttpClient) {

    this.loginForm = this.formBuilder.group({
      username: [''],
      password: ['']
    })
  }
  ngOnInit() {
  }


  doLogin() {
    /**
     * Calling service method by sending the form details.
     */
    this.service.doLogin(this.loginForm.value).subscribe((data) => {
      console.log(data);
      this.userData=data.response;
      if (data.statusCode == 1){
      console.log(this.userData)
        //Storing access token in localstorage
        localStorage.setItem('token', data.jwtAuthenticationResponse.accessToken);

        //Storing username in localstorage
        localStorage.setItem('name', data.response.username);

        //Storing userrole in localstorage
        localStorage.setItem('role', data.response.role);

        //Checking for user role
        if (data.response.role == "ROLE_USER") {
          //Navgating to userdetails component
          window.location.href = "/userDetails";
        }
        //Checking for admin role
        else if (data.response.role == "ROLE_ADMIN") {
          //Navgating to users component
          window.location.href = "/users"
        }
      }
      else {
        alert("please Enter Correct credentials..!");
      }
    })
  }
}
