//Npm dependencies
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

/**
 * This component holds the data to be displayed in footer.
 */
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }
}
