//Npm dependencies
import { Component, OnInit }       from '@angular/core';

//service imports
import { ServiceService }          from '../service.service';

@Component({
  selector: 'app-sorting',
  templateUrl: './sorting.component.html',
  styleUrls: ['./sorting.component.css']
})

/**
 * This component holds the sorted users list.
 */
export class SortingComponent implements OnInit {

  sortedData = new Array();
  constructor(private service: ServiceService) { }

  ngOnInit() {
    
    this.sortedData = this.service.sendNames();
    console.log(this.sortedData)
  }
 

 
}
