//Npm dependencies
import { Component, OnInit }        from '@angular/core';

//service imports
import { ServiceService }           from '../service.service';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})

/**
 * This component displays the success message.
 */
export class SuccessComponent implements OnInit {
  userName: string;
  constructor(private service: ServiceService) { }

  ngOnInit() {
    //Calling service method to get the username.
    this.userName = this.service.getUserName();
  }

}
