//npm dependencies
import { Component }                 from '@angular/core';
import { FormGroup, FormBuilder }    from '@angular/forms';
import { Router }                    from '@angular/router';

//service imports
import { ServiceService }            from '../service.service';

@Component({
  selector: 'app-set',
  templateUrl: './set.component.html',
  styleUrls: ['./set.component.css']
})

/**
 * This component sets the department to an user.
 */
export class SetComponent {
  Form: FormGroup;
  constructor(private formBuilder: FormBuilder, private service: ServiceService, private routes: Router) {
    this.Form = this.formBuilder.group({
      id: [''],
      department: ['']
    })
  }

  /**
   * This method assigns the department to user.
   */
  Assign() {
    //Calling service method for updating the department .
    this.service.updateDepartment(this.Form.value).subscribe((data) => {
      if (data.statusCode == 1) {
        alert("Department updated")
      }
    })
  }

}
