// Npm dependencies
import { Component, OnInit } from '@angular/core';

// Service imports
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

/**
 * This component holds the side menu part.
 */
export class MenuComponent implements OnInit {
  //getting name from local storage.
  userId: string = localStorage.getItem("name");
  //getting role from local storage.
  role: string = localStorage.getItem('role');
  userRole = false;
  adminRole: boolean = false;
  isLoggedIn: boolean = false;
  constructor(private service: ServiceService) { }

  ngOnInit() {
    //getting name from local storage.
    if (localStorage.getItem('name')) {
      this.isLoggedIn = true;
    }

    if (this.role == "ROLE_USER") {
      this.userRole = true;
    }

    if (this.role == "ROLE_ADMIN") {
      this.adminRole = true;
    }

  }
/**
 * This method is for logout.
 */
  doLogout() {
    if (window.confirm("Do you really wanna Logout")) {
      //Removing name from local storage.
      localStorage.removeItem('token');
      //Removing name from local storage.
      localStorage.removeItem('name');
      //Removing name from local storage.
      localStorage.removeItem('role');
      //navigating to login page.
      window.location.href = "/login";
    }
  }

}
